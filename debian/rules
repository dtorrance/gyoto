#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

include /usr/share/dpkg/architecture.mk

# Hardened flags
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
-include /usr/share/dpkg/buildflags.mk
CPPFLAGS += -I$(CURDIR)/debian/include

# Don't accidentally build the lorene plug-in
unexport HOME_LORENE

# Make sure we build with the right MPI implementation
include /usr/share/mpi-default-dev/debian_defaults
MPICXX := /usr/bin/mpic++.$(ARCH_DEFAULT_MPI_IMPL)
export MPICXX
CXX := $(shell readlink -e $(shell which $(firstword $(shell $(MPICXX) -show))))

# For python*-gyoto.install
DEB_GYOTO_SOVERS := $(shell expr \
	  $(shell grep VERSINFO_C= configure.ac | sed s/VERSINFO_C=//) \
	- $(shell grep VERSINFO_A= configure.ac | sed s/VERSINFO_A=//))
export DEB_GYOTO_SOVERS

configure_flags = --with-yorick=/usr/bin/yorick --with-udunits --with-xerces \
	--enable-static --enable-shared \
	--with-boost --with-arblib --enable-c++11 --enable-release \
	--with-mpi --without-virtualenv --enable-doc \
	--with-lorene=/usr/lib/$(DEB_HOST_MULTIARCH)/lorene \
	--includedir=/usr/include/$(DEB_HOST_MULTIARCH)

%:
	dh $@ --with python3,autoreconf

override_dh_auto_install-arch:
ifneq ($(wildcard plugins/python/Makefile),)
	make -C plugins/python distclean
endif
	dh_auto_install -a -- SUBDIRS="lib bin doc yorick"
	sh debian/install_for_all_pythons
	dh_numpy3 -a

override_dh_auto_install-indep:

override_dh_install-arch:
	dh_install -a -N yorick-gyoto
	dh_installyorick --no-make-install -pyorick-gyoto
	dh_makeshlibs -a

override_dh_install-indep:
	dh_install -i

override_dh_auto_build-arch:
	$(MAKE) COPT_DEFAULT="" \
	     Y_CFLAGS="$(CFLAGS) $(CPPFLAGS)" \
	     Y_LDFLAGS="$(LDFLAGS)" \
	     lib bin yorick
	for badflag in -g -O2 -Werror=format-security -Wformat ; do \
	 sed "s/ $${badflag}//" -i lib/gyoto.pc ; \
	done
	mkdir -p python-plugins
	set -e; \
	for python in `py3versions -s`; do \
	 make -C python PYTHON=$$python ; \
	 make -C plugins/python distclean ; \
	 cd plugins/python ; \
	 ./configure PYTHON=`which $$python` ; \
	 make ; \
	 cd ../../ ; \
	 cp plugins/python/lib/.libs/libgyoto-$$python.so python-plugins/ ; \
	done
	cd python-plugins ; \
	set -e; \
	for python in python3 ; do \
	  ln -s libgyoto-`which $${python} | xargs readlink | xargs basename`.so libgyoto-$${python}.so ; \
	done

override_dh_auto_build-indep:
	$(MAKE) doc

override_dh_auto_configure:
	if sha1sum -c debian/boost.sha1sums; then \
		mkdir -p debian/include/boost/multiprecision/detail/functions/;\
		cp /usr/include/boost/multiprecision/detail/functions/trig.hpp \
			debian/include/boost/multiprecision/detail/functions/;\
		cd debian/include; \
		patch -p1 < ../patches/no-fp-ilogb0;\
		echo "************ Patching Boost.multiprecision ************";\
	else \
		echo "********** NOT Patching Boost.multiprecision **********";\
	fi
	dh_auto_configure -- $(configure_flags)
	sed -i 's|^CXX=.*|CXX=$(CXX)|' lib/gyoto.pc
	sed -i 's/ -fdebug-prefix-map=\S*//' lib/gyoto.pc

override_dh_makeshlibs:
	dh_makeshlibs -Xstdplug -Xpython

override_dh_auto_clean:
	[ ! -f python/Makefile ] || make -C python clean
	rm -Rf python-plugins debian/include python/_*.so python/gyoto/_*.so
	dh_auto_clean --no-parallel

override_dh_auto_test-indep:

override_dh_auto_test-arch:
ifeq (,$(filter $(DEB_HOST_ARCH), mips mips64el))
# skip on mips as lucatelli2 fails on it (but the binary works on other boxes)
#
# 20200228: For some reason the behavior of setup.py test changed to
# that we need to put all the .so files in python/gyoto. This should
# be fixed upstream in a better manner.
#
# 20200309: skip mips64el due to segfault in libflint-arb2 (#953437)
	cp -a python/build/*/gyoto/_*.so python/gyoto/
	LC_ALL=C.UTF-8 dh_auto_test -- check-lorene
endif

#!/bin/sh
set -e

# Name of this script
script=`basename "$0"`

# Handle case of manually running this script
if [ "$AUTOPKGTEST_TMP" = "" ]; then
    AUTOPKGTEST_TMP=`mktemp -d --tmpdir "$script"-XXX`;
    echo "AUTOPKGTEST_TMP is $AUTOPKGTEST_TMP";
fi
if [ "$ADT_ARTIFACTS" = "" ] ; then
    ADT_ARTIFACTS=${AUTOPKGTEST_TMP}/artifacts
fi
mkdir -p "${AUTOPKGTEST_TMP}"
mkdir -p "${ADT_ARTIFACTS}"

# Save current directory (must be top of source tree)
SRC_ROOT=`pwd`

# Compute Lorene metric using Nrotstar "code"
export HOME_LORENE=/usr/lib/`dpkg-architecture -qDEB_HOST_MULTIARCH`/lorene
mkdir -p ${AUTOPKGTEST_TMP}/${script}
cd ${AUTOPKGTEST_TMP}/${script}
rm -Rf .check-lorene
cp -a ${HOME_LORENE}/Codes/Nrotstar ./.check-lorene
cd .check-lorene
rm -f nrotstar *.o
cp -f Parameters/GR/Kepler/*.d ./
sed -i.bak 's/1       graph/0       graph/' par_rot.d
make
./nrotstar
cd ..

# Get list of Lorene examples
EXAMPLES_DIR=${SRC_ROOT}/doc/examples
BASE_NAMES=`basename -s.xml ${EXAMPLES_DIR}/*-rotstar3_1.xml`

# Build Gyoto command line
GYOTO="/usr/bin/gyoto --nprocesses=0 --nthreads=1 --resolution=32 --plugins=stdplug,lorene"

# Run each example in this environment
for base in ${BASE_NAMES} ; do
    ${GYOTO} \
		  ${EXAMPLES_DIR}/${base}.xml \
		  \!${ADT_ARTIFACTS}/${base}.fits ;
done

exit 0
